defmodule Splurty.Quote do
  use Splurty.Web, :model

  import Ecto.Query
  alias Splurty.Repo
  alias Splurty.Quote

  schema "quotes" do
    field :saying, :string
    field :author, :string

    timestamps
  end

  @required_fields ~w(saying author)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def random do
    hd from(q in Quote, select: q, limit: 1, order_by: fragment("RANDOM()")) |> Repo.all
  end
end

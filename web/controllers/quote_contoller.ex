defmodule Splurty.QuoteController do
  use Splurty.Web, :controller

  alias Splurty.Quote

  def homepage(conn, _params) do
    conn
    |> assign(:quote, Quote.random)
    |> render "show.html"
  end

  def index(conn, _params) do
    conn
    |> assign(:quotes, Repo.all(Quote))
    |> render("index.html")
  end

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, %{"quote" => quote_params}) do
    changeset = Quote.changeset %Quote{}, quote_params
    Repo.insert changeset

    redirect conn, to: quote_path(conn, :index)
  end

  def show(conn, %{"id" => id}) do
    conn
    |> assign(:quote, Repo.get!(Quote, id))
    |> render("show.html")
  end

  def edit(conn, %{"id" => id}) do
    conn
    |> assign(:quote, Repo.get!(Quote, id))
    |> render("edit.html")
  end

  def update(conn, %{"id" => id, "quote" => quote_params}) do
    changeset = Quote.changeset Repo.get!(Quote, id), quote_params
    Repo.update changeset

    redirect conn, to: quote_path(conn, :show, id)
  end

  def delete(conn, %{"id" => id}) do
    quote = Repo.get! Quote, id
    Repo.delete quote

    redirect conn, to: quote_path(conn, :index)
  end
end
